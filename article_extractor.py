# coding: utf-8
import re
import requests
import facebook
from datetime import datetime, timedelta
import time
from bs4 import BeautifulSoup
import spacy
import justext
import asks
import time
import trio



bing_subscription_key = '913d7c2289cf41b2beb03e1bac5eaea7'
search_url = "https://centralindia.api.cognitive.microsoft.com/bing/v7.0/news/search"

def get_social_counts(url):
    '''
    Get the article counts for social-related actions, i.e.: shares, comments, etc
    '''
    social_counts_data = {
        'facebook':
            {'engagement':
                {'reaction_count': 0,
                    'comment_count': 0,
                    'share_count': 0,
                    'comment_plugin_count': 0},
                'id': url}
    }

    try:
        graph = facebook.GraphAPI(access_token="604755649955196|-FPhAMtR_3ysVFRVwkacih2P0Ww", version="2.12")
        response = graph.get_object(id=url, fields='engagement')
        if 'engagement' in response:
            social_counts_data['facebook'] = response
        return social_counts_data

    except Exception as e:
        print(e)
        return social_counts_data


def get_top_headlines(search_term, n_articles=20, start_period = None, market = "en-US"):
    '''
    Obtain the most recent news articles related to a search term using Microsoft Bing News API
    '''
    headers = {"Ocp-Apim-Subscription-Key" : bing_subscription_key}
    params  = {"q": search_term, "textDecorations": True, "textFormat": "HTML" , "mkt" : market, "count" : n_articles}

    if start_period:
        params['sortBy'] = 'Date'
        params['since'] = get_unix_start_date(start_period)
    try:
        response = requests.get(search_url, headers=headers, params=params)
        response.raise_for_status()
        search_results = response.json()
        return search_results

    except Exception as e:
        print(e)
        return {}

def spacy_tokenize(txt, token_min_len, token_max_len):
    return [t.text.lower() for t in nlp(txt) if len(t.text) <= token_max_len and len(t.text) >= token_min_len]


def get_url_contents(urls, verbose=False):
    '''
    Takes a list of URLs, requests the contents using concurrency and returns a list with their contents.
    '''
    asks.init("trio")
    async def get_urls_parallel(url_l):
        results = {}
        async with trio.open_nursery() as nursery:
            for i, url in enumerate(url_l):
                nursery.start_soon(fetch_url, url, results, i)
        return results

    async def fetch_url(url, results, i):
        try:
            if verbose:
                print("Start: ", url)
                response = await asks.get(url)
                print("Finished: ", url, len(response.content), response.status_code)
            else:
                response = await asks.get(url)
            if response.status_code == 200:
                results[url] = response.content
        except Exception as e:
            print(e)
    r = trio.run(get_urls_parallel, urls)
    return r


def get_clean_txt(raw_article_html, num_paragraphs=None):
    '''
    Takes the raw HTML markup from an article and extracts the cleansed article text.
    '''
    paragraphs = justext.justext(raw_article_html, justext.get_stoplist("English"))
    text = ""
    for i, paragraph in enumerate(paragraphs):
        if num_paragraphs and i == num_paragraphs:
            break
        if not paragraph.is_boilerplate:
            text+=paragraph.text
    return text


def get_news_articles_summary_data(search_term, n_articles, exclude_url):
    '''
    -Obtains a list of articles relevant to the search_term from Bing News API
    -Obtains the associated social media reach data-points (reactions, shares, comments)
    -Cleans and downloads the article body.
    -Returns a list containing a summary data dictionary for each article.
    '''
    article_list = get_top_headlines(search_term, n_articles)
    url_list = [(x['url']) for x in article_list['value'] if x['url'] != exclude_url]
    contents = get_url_contents(url_list, verbose=False)
    summary = []
    for url in url_list:
        if url in contents:
            full_txt = get_clean_txt(contents[url])
        else:
            full_txt = ''
        if full_txt != '':
            social = get_social_counts(url)
            summary.append({'url':url, 'social':social, 'full_txt':full_txt})
    summary = sorted(summary, key=lambda x: int(x['social']['facebook']['engagement']['share_count']), reverse = True)
    return summary


def get_unix_start_date(period_str):
        now = datetime.today()
        if period_str == '1m':
            dt = now - timedelta(days=30)
        elif period_str == '3m':
            dt = now - timedelta(days=90)
        elif period_str == '6m':
            dt = now - timedelta(days=180)
        else:
            dt = now - timedelta(days=365)
        unix_time = int(time.mktime(dt.timetuple()))
        return unix_time
