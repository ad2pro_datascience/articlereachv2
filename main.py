from gensim.summarization import keywords
import sys
import gensim
import pandas as pd
import nltk
nltk.download('stopwords')
from sklearn.feature_extraction.text import TfidfVectorizer
from collections import Counter
import string

from gensim import corpora
from gensim.models import TfidfModel
from gensim.models import LsiModel
from gensim.similarities import MatrixSimilarity

from nltk.corpus import wordnet
import article_extractor
import spacy
import textacy
from textacy import keyterms
import re

SPACY_NLP = spacy.load('en_core_web_sm')

def get_wordnet_pos(treebank_tag):

        if treebank_tag.startswith('J'):
                return wordnet.ADJ
        elif treebank_tag.startswith('V'):
                return wordnet.VERB
        elif treebank_tag.startswith('N'):
                return wordnet.NOUN
        elif treebank_tag.startswith('R'):
                return wordnet.ADV
        else:
                return ''

translator = dict.fromkeys(string.punctuation)
#stopwords_list = [str(x).translate(None , string.punctuation) for x in nltk.corpus.stopwords.words('english')]
stopwords_list = [str(x).translate(string.punctuation) for x in nltk.corpus.stopwords.words('english')]

lemmatizer = nltk.stem.WordNetLemmatizer()
tokenizer = nltk.tokenize.RegexpTokenizer('\w+')
lemmatized = lambda x: [lemmatizer.lemmatize(xx,get_wordnet_pos(pos)) if get_wordnet_pos(pos) else lemmatizer.lemmatize(xx) for xx , pos in nltk.pos_tag(x.lower().split())]
cleaned_file = lambda x: [xx for xx in lemmatized(x) if str(xx).translate(string.punctuation) not in stopwords_list]
return_nouns = lambda x: [filter_word for filter_word , filter_tag in nltk.pos_tag(x) if 'NN' in filter_tag ]
decoder = lambda sent : ''.join(char for char in sent if ord(char)<128)


def get_keywords_for_article(article_to_estimate):
    try:
        article_to_estimate = decoder(article_to_estimate)
        cleaned_article = cleaned_file(article_to_estimate)
        keywords = extract_keywords(article_to_estimate, 50)

        if keywords:
            all_sentences = [x for x in filter(lambda x:x,' '.join(cleaned_article).split('\n'))] #check if cleaned_article makes sense or the raw article_to_estimate
            vect = TfidfVectorizer(ngram_range=(1, 3),stop_words=nltk.corpus.stopwords.words('english'))
            sentences_tfidf = vect.fit_transform(all_sentences)
            df_tfidf = pd.DataFrame(sentences_tfidf.toarray(),columns=vect.get_feature_names())
            keywords_from_tfidf = list(df_tfidf.mean().sort_values(ascending=False)[:10].index)
            keywords_from_tf = [x for x,y in Counter(cleaned_article).most_common(5)]
            return {'keywords':keywords,'keywords_from_tfidf':return_nouns(keywords_from_tfidf),'keywords_from_tf':keywords_from_tf}
        else:
            print ('There was no keywords extracted    ' )
            return {'keywords':[],'keywords_from_tfidf':[],'keywords_from_tf':[]}
    except Exception as e :
        print ('Excepted' , e)
        return {'keywords':[],'keywords_from_tfidf':[],'keywords_from_tf':[]}


def get_sim_res(data, n_articles=20, exclude_url=None):
    #keywords = get_keywords_for_article(data)
    #data = ''.join(x for x in data if ord(x) < 128)
    #search_term = set(keywords['keywords']).union(set(keywords['keywords_from_tfidf']))
    search_term = extract_keywords(data)[:10]
    search_phrase = ' '.join(search_term)[:125]
    #print("search = %s" %search_phrase)
    results = article_extractor.get_news_articles_summary_data(search_phrase, n_articles=n_articles, exclude_url=exclude_url)
    return results

def return_lsi_similarity_prob(results,article_to_estimate):
    '''
    For the similar results extracted from Bing News api and the input article , a LSI similarity matrix is built for all the results extracted
    The input blog is compared against the articles extracted and a sim score is given from -1 to 1 in the vec_lsi variable
    '''
    tokenizer_words = nltk.tokenize.RegexpTokenizer('[a-zA-Z]+')
    lemmatized_words = lambda x: [lemmatizer.lemmatize(xx,get_wordnet_pos(pos)) if get_wordnet_pos(pos) else lemmatizer.lemmatize(xx) for xx , pos in nltk.pos_tag(tokenizer_words.tokenize(x.lower()))]
    clean_words = lambda x: [xx for xx in lemmatized_words(str(decoder(x)).translate(string.punctuation)) if xx not in stopwords_list]
    all_documents = [clean_words(x['full_txt']) for x in results]
    dictionary = corpora.Dictionary(all_documents)
    corpus_gensim = [dictionary.doc2bow(doc) for doc in all_documents]

    tfidf = TfidfModel(corpus_gensim)
    corpus_tfidf = tfidf[corpus_gensim]
    lsi = LsiModel(corpus_tfidf, id2word=dictionary, num_topics=200)
    lsi_index = MatrixSimilarity(lsi[corpus_tfidf])

    new_doc_vec = dictionary.doc2bow(clean_words(article_to_estimate))
    vec_lsi = lsi[new_doc_vec]
    sims = lsi_index[vec_lsi]
    return list(sims)

def return_entities_keywords_percent(results,article_to_estimate):
    '''
    For the similar results extracted from Bing news API and the input article , the keywords and entities returned from the api is obtained
    The results are said to be more similar to the article or blog if they share a few keywords or entities
    '''
    l = []
    base_article_keywords = filter_keywords(extract_keywords(article_to_estimate))
    base_article_entities = filter_keywords(extract_entities(article_to_estimate))
    base_article_entities_keywords = set(base_article_keywords).union(set(base_article_entities))
    common_ents_kws = []
    different_ents_kws = []
    hashtags = []
    for i, ind_res in enumerate(results):
        if ind_res['full_txt'] != '':
            news_article_keywords = filter_keywords(
                                    extract_keywords(ind_res['full_txt']))
            article_hashtags = generate_hasthtags_from_keyword_list(
                                   news_article_keywords[:10])
            news_article_entities = filter_keywords(
                                    extract_entities(ind_res['full_txt']))
            news_article_entities_keywords = set(
                news_article_keywords).union(set(news_article_entities)
            )
            common_entities_keywords = news_article_entities_keywords.intersection(base_article_entities_keywords)
            try:
                entities_kws_match_percent = len(common_entities_keywords)/float(len(base_article_entities_keywords))
            except ZeroDivisionError:
                entities_kws_match_percent =  0
            common_ents_kws.append((common_entities_keywords))
            diff_keywords = news_article_entities_keywords.difference(set(common_entities_keywords))
            different_ents_kws.append((diff_keywords))
            l.append(entities_kws_match_percent)
            hashtags.append(article_hashtags)
    return (l , common_ents_kws , different_ents_kws, hashtags)


def generate_hasthtags_from_keyword_list(kw_list):
    return ['#'+kw.replace(' ', '') for kw in kw_list]


def filter_keywords(keywords):
    filtered_list = [w.encode("ascii", errors="ignore").decode() for w in keywords if len(w) >= 3 and not has_numbers(w) and not has_punctuation(w)]
    return filtered_list[:10]


def has_numbers(str):
    num_list = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
    return any(char.isdigit() for char in str) or (str in num_list)


def has_punctuation(str):
    punctuation_list = [',', '"', '!', '?', '.']
    return any(p in str for p in punctuation_list)


def get_final_similarity(article_to_estimate, results , sim_weight , enty_weight):
    lsi_sim = return_lsi_similarity_prob(results , article_to_estimate)
    ents_kws_sim , common_ents_kws , different_ents_kws, hashtags = return_entities_keywords_percent(results , article_to_estimate)

    l = []

    for lsi_s , ents_s , common_e_k , diff_e_k, hashtag_list , res in zip(lsi_sim , ents_kws_sim , common_ents_kws , different_ents_kws, hashtags , results):
        final_score = lsi_s * sim_weight + ents_s * enty_weight
        l.append((res['url'] , common_e_k , diff_e_k , hashtag_list, res['social']['facebook']['engagement']['share_count'], final_score))
    max_score = max(l,key=lambda x:x[-1])[-1]
    #filtered_list = filter(lambda x:x[-1]>0.8*max_score , l )
    #return sorted(filtered_list,reverse=True,key=lambda x:x[-1])#, l l has the unfiltered
    filtered_list = [a for a in l if a[0] != ""]
    return sorted(filtered_list,reverse=True,key=lambda x:x[-1])#, l l has the unfiltered


def extract_keywords(text, spacy=SPACY_NLP, n_keywords = 30):
    '''Takes a body of text and returns a list of keywords using the sgrank algorithm'''
    #text = remove_punctuation(text, SPACY_NLP)
    doc = spacy(text)
    keywords = textacy.keyterms.sgrank(doc, ngrams=(1, 2, 3), normalize='lower', n_keyterms=int(2*n_keywords/3))
    try:
        extra_keywords = textacy.keyterms.textrank(doc, normalize='lemma', n_keyterms=int(n_keywords/3))
    except:
        extra_keywords = []
    return [kw[0] for kw in keywords+extra_keywords]


def extract_entities(text, spacy=SPACY_NLP):
    '''Takes a body of text and returns a list of named entities using spaCy entities extraction'''
    doc = spacy(text.lower())
    return [t.text for t in doc.ents]


def remove_punctuation(txt, nlp):
    doc = nlp(txt.lower())
    #tokens = [token.text for token in doc if not token.text in SPACY_NLP.Defaults.stop_words and not token.is_punct]
    tokens = [token.text for token in doc if not token.is_punct]
    return " ".join(tokens)
