import main
import pandas as pd
import sys
import article_extractor
import requests

def drive_from_path(file_path):
    with open(file_path, 'r', encoding='latin1') as f:
        data = f.read().replace('\n', '')
    data = ''.join(x for x in data if ord(x) < 128)
    get_filtered_results(data)


def drive_from_url(url):
    r = requests.get(url)
    if r.status_code == 200:
        article_html = article_extractor.get_url_contents([url])
        article_full_txt = article_extractor.get_clean_txt(article_html[url])
        return get_filtered_results(article_full_txt, url)
    else:
        print('\nBad URL, please check and try again.\n')


def get_filtered_results(data, exclude_url=None):
    results = main.get_sim_res(data, n_articles=20, exclude_url=exclude_url)
    filtered_res = main.get_final_similarity(data, results,
                                 sim_weight=0.6, enty_weight=0.4)
    df = pd.DataFrame(list(zip(*filtered_res))).T
    df.columns = ['body', 'common_keywords',
                  'different_keywords','hashtags', 'social_shares', 'similarity_score']
    df.to_csv('all_results.csv', index=False, encoding='utf8')
    print('Results dumped to all_results.csv')
    return filtered_res


if __name__ == '__main__':
    '''
    Usage python driver.py -f path_to_file
    Usage python driver.py -url url_to article
    '''
    if sys.argv[1] == '-url':
        try:
            url = str(sys.argv[2])
        except Exception as e:
            print(e)
        drive_from_url(url)

    elif sys.argv[1] == '-f':
        try:
            path = str(sys.argv[2])
        except Exception as e:
            print(e)
        drive_from_path(path)
